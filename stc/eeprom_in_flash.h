/* eeprom in flash
 * Copyright (c) 2021-2022 epoko
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#ifndef _EEPROM_IN_FLASH_H_
#define _EEPROM_IN_FLASH_H_
#include "main.h"

/* eeprom storage data num max, 1-255, unit byte.
Must be smaller than (EEPROM_PART_SIZE/2)-2 */
#define EEPROM_NUM_MAX          (4)

/* EEPROM Use two partitions, each partition size is
an integer multiple of the erased page */
#define IAP_OFFSET 0x2000
#define EEPROM_PART0_SIZE       (2048)  //eeprom part size 2k, total size 4k
#define EEPROM_PART1_SIZE       (EEPROM_PART0_SIZE)

/* EEPROM start address in Flash */
#define EEPROM_START_ADDRESS    (0x0000)
#define EEPROM_END_ADDRESS      (0x0000 + EEPROM_PART0_SIZE + EEPROM_PART1_SIZE)

/* Pages 0 and 1 base and end addresses */
#define PART0_BASE_ADDRESS      (EEPROM_START_ADDRESS)
#define PART0_END_ADDRESS       ((PART0_BASE_ADDRESS + EEPROM_PART0_SIZE))

#define PART1_BASE_ADDRESS      (PART0_END_ADDRESS)
#define PART1_END_ADDRESS       ((PART1_BASE_ADDRESS + EEPROM_PART1_SIZE))

/* PAGE is marked to record data */
#define PART_USED_MARK          (0xDA15 + EEPROM_NUM_MAX)	//Different number, use new data
//#define PART_USED_MARK          (0xDA15)  //Different number, use old data

#ifndef log_printf
	#define log_printf printf
#endif

#ifndef HAL_OK
	#define HAL_OK (0)
	#define HAL_ERROR (1)
#endif

uint8_t EEPROM_Init(void);
uint8_t EEPROM_Read(uint8_t Address);
uint8_t EEPROM_Write(uint8_t Address, uint8_t Data);
uint16_t EEPROM_ReadWORD(uint8_t Address);
uint8_t EEPROM_WriteWORD(uint8_t Address, uint16_t Data);

#endif
